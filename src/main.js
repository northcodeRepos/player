import Vue from 'vue';
window._ = require('lodash');

import Player from './components/player.vue';


new Vue({
    data: function () {
        return {
            message: 'Webpack and Vue setup'
        }
    },
    components: {
        Player
    },
    el: '#app'
});