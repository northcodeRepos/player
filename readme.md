Music Player Project

Do obsługi projektu potrzebujemy zainstalować Node.js, Yarn, Gulp-cli oraz konsolę komend najlepiej cmdr .
- https://nodejs.org/en/
- https://yarnpkg.com/latest.msi
- http://cmder.net/
- https://www.npmjs.com/package/gulp-cli

1. Odpalamy konsolę
2. Pobieramy repozytorium komendą ,, git clone https://filmomaniak@bitbucket.org/filmomaniak/player.git ,,
3. Przechodzimy do pobranego katalogu
4. Instalujemy komendę ,, Yarn ,, z adresu https://yarnpkg.com/latest.msi
5. W konsolę wpisujemy ,, yarn ,, następnie czekamy aż wszystko się zainstaluje.
6. Instalujemy gulp komendą ,, npm i gulp-cli -g ,, a projekt odpalamy wpisując ,, gulp ,, w konsoli .
7. Projekt jest dostępny teraz pod adresem http://localhost:6555/ 